package com.qgk.espect;

class Derived{
    public void getDetails(){
        System.out.printf(" Derivred Class");
    }
}
public class Test9 extends Derived{
    @Override
    public void getDetails() {
        System.out.printf("Test class");
        super.getDetails();
        System.out.printf("Test2 class");
    }

    public static void main(String[] args) {
        Derived derived = new Test9();
        derived.getDetails();
    }
}
