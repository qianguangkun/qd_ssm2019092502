package cn.mldn.demo12.c01;


class Outer{
    private String msg = "www.mldn.cn";

    public void fun(){
        Inner in = new Inner();
        in.print();
    }

    class Inner{
        public void print(){
            System.out.println(Outer.this.msg);
        }
    }

}

public class JavaDemo01 {
    public static void main(String[] args) {
        Outer outer = new Outer();
        outer.fun();
    }
}
