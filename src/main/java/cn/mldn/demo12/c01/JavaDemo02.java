package cn.mldn.demo12.c01;


class Outter{
    private String msg = "www.mldn.cn";
    public void fun(){
        Inner in = new Inner();
        in.print();
    }

    //思考1
    public String getMsg() {
        return msg;
    }
}


class Inner{


    public void print(){
        //思考2
        System.out.println("");
    }
}


public class JavaDemo02 {
}
