


class Node{
    Node next = null;

    int data;

    public Node(int data) {
        this.data = data;
    }
}


public class MyLinkedList {

    Node head = null; //链表头的引用

    /**
     * 向链表中插入数据  尾插法
     * @param d :插入内容
     */
    public void addNode(int d) {
        Node newNode = new Node(d);
        if (head == null) {
            head = newNode;
            return;
        }

        Node tmp = head;
        while (tmp.next != null) {
            tmp = tmp.next;
        }

        //add node to end
        tmp.next = newNode;
    }

    /**
     *
     * @return 返回结点的长度
     */
    public int length() {
        int length = 0;
        Node tmp = head;
        while (tmp != null) {
            length++;
            tmp = tmp.next;
        }
        return length;
    }

    /**
     *
     * @param index:删除第index个元素
     * @return 成功返回true 失败返回else
     */
    public boolean deleteNode(int index) {

        //删除位置不合理
        if (index < 1 && index > length()){
            return false;
        }

        //删除第一个元素
        if (index == 1) {
            head = head.next;
            return true;
        }

        int i = 2;
        Node preNode = head;
        Node curNode = preNode.next;
        while (curNode != null) {
            //找到要删除的元素索引时
            if (i == index) {
                preNode.next = curNode.next;
                return  true;
            }
            preNode = curNode;
            curNode = curNode.next;
            i++;
        }
        return true;
    }

    /**
     * 对链表顺序排序
     * @return 返回排序后头结点
     */
    public Node ordList(){

        Node nextNode = null;
        int temp = 0;
        Node curNode = head;
        while (curNode.next != null) {
            nextNode =curNode.next;
            while (nextNode != null) {
                //符合目的的
                if (curNode.data > nextNode.data) {
                    temp = curNode.data;
                    curNode.data = nextNode.data;
                    nextNode.data = temp;
                }
                //继续查找
                nextNode = nextNode.next;
            }
            curNode = curNode.next;
        }
        return head;
    }

    public void printList(){
        Node tmp = head;
        while (tmp!=null){
            System.out.println(tmp.data);
            tmp = tmp.next;
        }
    }

    public static void main(String[] args) {
        MyLinkedList list = new MyLinkedList();
        list.addNode(5);
        list.addNode(3);
        list.addNode(1);

        System.out.println("listLen="+list.length());
        System.out.println("before Order");
        list.printList();
        list.ordList();
        System.out.println("after Order");
        list.printList();

    }

}
